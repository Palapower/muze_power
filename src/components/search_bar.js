import React, {Component} from 'react';

// const Searchbar = function(){
//   return <input placeholder="search" />;
// }

class Searchbar extends Component {

  constructor(props) {
    super(props);

    this.state = {term : ''};
    this.str ={term : ''};
  }

  render() {
    return (
      <div>
        <input placeholder="search"
          value = {this.state.term}
          onChange={event => this.onInputChange(event.target.value)} />
      </div>
    )
  }

  onInputChange(term){
    this.setState({term});
    this.props.onSearchTermChange(term);
  }

  // onChangeInput(event) {
  //   // console.log(event.target.value);
  //   setState(event.target.value);
  // }
}

export default Searchbar;
